"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const answersSchema = new Schema(
  {
    value: {
      type: String,
      required: true,
    },
    question: {
      type: "ObjectId",
      ref: "Questions",
      select: false,
    },
    user: {
      type: "ObjectId",
      ref: "Users",
      select: false,
    },
  },
  {
    collection: "answers",
    versionKey: false,
  }
);

answersSchema.index({ question: 1 });

module.exports = mongoose.model("Answers", answersSchema);
