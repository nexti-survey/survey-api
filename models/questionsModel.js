"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const questionsSchema = new Schema(
  {
    question: {
      type: String,
      required: true,
    },
    answerType: {
      type: String,
      required: true,
      enum: ["String", "Number", "Boolean"],
      default: "String",
    },
    answers: [
      {
        type: "ObjectId",
        ref: "Answers",
      },
    ],
  },
  {
    collection: "questions",
    versionKey: false,
  }
);

module.exports = mongoose.model("Questions", questionsSchema);
