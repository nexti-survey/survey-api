# [Survey API](https://api.survey.nexti.dev)

API designed to the Survey project.

There is a file called `insomniaCollection.json` that contains all the available requests.
You can [download](https://insomnia.rest/download) the application, import this collection and try the requests.

## Features

* Question and Answers CRUD
* User CRUD with JWT Authentication

## Methods 

### Users:
---

`POST /users` - Creates a new user

**body**: JSON

```
{
	"firstName": "Survey",
	"lastName": "Test",
	"password": "survey",
	"username": "survey"
}
```

---

`GET /users` (*authenticated only*) - Gets the user info through the JWT Token


**header**

`x-access-token`=`JWT Token`

---

`POST /users/login` - Authenticates the user

**body**

```
{
	"username": "survey",
	"password": "survey"
}
```

---

### Questions
---

`POST /questions` (*authenticated only*) - Creates a new question

**body**

```
{
	"question": "Is a hot dog a sandwich? Why?",
	"answerType": "String"
}
```
---
**Answer Types**:

* String
* Number
* Boolean

---

`POST /questions/:questionId/answers` (*authenticated only*) - Adds an answer to the question

**body**

```
{
	"value": "Yes, because it has bread and a kind of meat inside"
}
```

---

`GET /questions` (*authenticated only*) - Retrieves all available questions

---

`GET /questions/:questionId` (*authenticated only*) - Retrieves the question details, with all answers

---

## Execution

---

To execute this project into your machine, you need to download the project, and follow the next steps:

* Download all dependencies:
  ```
  ~$ yarn 
  ```

* Copy the .env_template:
  ```
  ~$ cp .env_template .env
  ```

* Edit the .env and replace all values to valid ones. Ex:
  ```
  NODE_ENV=production
  MONGO_HOST=host.mongodb.net
  MONGO_USER=user
  MONGO_PWD=user
  PORT=3100
  SECRET=jwt-token
  SALT_WORK_FACTOR=10
  ```

* Now you can execute it:
  ```
  ~$ yarn start
  ```
* It will print the following message:
  ```
  Survey API server started on: 3100
  ```

Then you are ready to go!
