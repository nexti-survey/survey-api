"use strict";
var mongoose = require("mongoose"),
  Questions = mongoose.model("Questions");

exports.getQuestions = async function (req, res) {
  try {
    const questions = await Questions.find({});

    return res.json(questions);
  } catch ({ message, ...error }) {
    return res.status(400).json({
      success: false,
      message,
    });
  }
};

exports.questionDetails = async function (req, res) {
  try {
    const question = await Questions.findById(req.params.questionId);

    if (typeof question === "undefined") {
      return res.status(404).json({
        success: false,
        message: `No question found with id ${req.params.questionId}`,
      });
    }

    return res.json(question);
  } catch ({ message, ...error }) {
    if (error.name === "CastError")
      return res.status(404).json({
        success: false,
        message: `No question found with id ${req.params.questionId}`,
      });
    else
      return res.status(400).json({
        success: false,
        message,
      });
  }
};

exports.createQuestion = async function (req, res) {
  try {
    const newQuestion = new Questions(req.body);

    const question = await newQuestion.save();

    res.json({
      success: true,
      question,
    });
  } catch ({ message }) {
    return res.status(400).json({
      success: false,
      message: `Cannot create a new question: ${message}`,
    });
  }
};

exports.updateQuestion = async function (req, res) {
  try {
    const question = await Questions.findByIdAndUpdate(
      req.params.questionId,
      req.body,
      {
        new: true,
      }
    );

    res.json({
      success: true,
      question,
    });
  } catch ({ message, ...error }) {
    if (error.name === "CastError")
      return res.status(404).json({
        success: false,
        message: `No question found with id ${req.params.questionId}`,
      });
    else
      return res.status(400).json({
        success: false,
        message: `Cannot update question: ${message}`,
      });
  }
};

exports.deleteQuestion = async function (req, res) {
  try {
    let questionDb = await Questions.findById(req.params.questionId);

    if (!questionDb) {
      res.status(404).json({
        status: false,
        message: `Question '${req.params.questionId} is not found`,
      });
    }

    await Questions.findByIdAndDelete(req.params.questionId);

    res.json({
      success: true,
    });
  } catch ({ message, ...error }) {
    return res.status(400).json({
      success: false,
      message: `Cannot delete question: ${message}`,
    });
  }
};
