"use strict";
const jwt = require("jsonwebtoken"),
  mongoose = require("mongoose"),
  Users = mongoose.model("Users");

const getUser = async function (req, res, next) {
  try {
    const user = await Users.findById(req.userId);

    if (typeof user === "undefined") {
      return res.status(404).json({
        success: false,
        message: `No user found with id ${req.userId}`,
      });
    }

    // Passing found user as req parameter
    req.user = user;

    if (next !== null) next();
  } catch ({ message, ...error }) {
    if (error.name === "CastError")
      return res.status(404).json({
        success: false,
        message: `No user found with id ${req.userId}`,
      });
    else
      return res.status(400).json({
        success: false,
        message,
      });
  }
};

exports.getUser = getUser;

exports.userDetails = async function (req, res) {
  await getUser(req, res, null);

  res.json(req.user);
};

exports.createUser = async function (req, res) {
  try {
    const newUser = new Users(req.body);

    let userDb = await Users.findOne({ username: newUser.username });

    if (userDb) {
      return res.status(400).json({
        status: false,
        message: `User '${newUser.username}' is already taken`,
      });
    }

    await newUser.save();

    let user = await Users.findOne({ username: newUser.username }).lean();

    res.json({
      success: true,
      user,
    });
  } catch ({ message }) {
    return res.status(400).json({
      success: false,
      message: `Cannot create a new user: ${message}`,
    });
  }
};

exports.updateUser = async function (req, res) {
  try {
    const user = await Users.findByIdAndUpdate(req.params.userId, req.body, {
      new: true,
    });

    res.json({
      success: true,
      user,
    });
  } catch ({ message, ...error }) {
    if (error.name === "CastError")
      return res.status(404).json({
        success: false,
        message: `No user found with id ${req.params.userId}`,
      });
    else
      return res.status(400).json({
        success: false,
        message: `Cannot update user: ${message}`,
      });
  }
};

exports.login = async function (req, res) {
  let message = "Username/Password don't match";

  const { username, password } = req.body;

  let user = await Users.findOne({ username }).select("password");

  if (!user) {
    return res.status(401).json({
      success: false,
      message: message,
    });
  }

  user.comparePassword(password, async function (err, isMatch) {
    if (!isMatch) {
      if (err) message = err.message;

      return res.status(401).json({
        success: false,
        message: message,
      });
    }

    const token = jwt.sign({ id: user._id }, process.env.SECRET, {
      expiresIn: 3600,
    });

    return res.json({ token });
  });
};

exports.checkToken = async function (req, res, next) {
  const token = req.headers["x-access-token"];

  if (!token)
    return res
      .status(401)
      .json({ success: false, message: "No token provided." });

  jwt.verify(token, process.env.SECRET, function (err, decoded) {
    if (err)
      return res
        .status(500)
        .json({ success: false, message: "Failed to authenticate token." });

    req.userId = decoded.id;

    if (next) return next();

    res.json({ success: true });
  });
};
