"use strict";
var mongoose = require("mongoose"),
  Questions = mongoose.model("Questions"),
  Answers = mongoose.model("Answers");

exports.reply = async function (req, res) {
  try {
    let questionDb = await Questions.findById(req.params.questionId);

    if (!questionDb) {
      res.status(404).json({
        status: false,
        message: `Question '${req.params.questionId} is not found`,
      });
    }

    const newAnswer = new Answers(req.body);
    newAnswer.question = req.params.questionId;
    newAnswer.user = req.userId;

    const answer = await newAnswer.save();

    await Questions.findByIdAndUpdate(req.params.questionId, {
      $push: { answers: answer._id },
    });

    res.json({
      success: true,
    });
  } catch ({ message }) {
    return res.status(400).json({
      success: false,
      message: `Cannot reply the question: ${message}`,
    });
  }
};

exports.getAnswers = async function (req, res) {
  try {
    let page = 1 || req.query.page;

    if (page == 0) page = 1;

    const answers = await Answers.find({ question: req.params.questionId })
      .skip((page - 1) * 100)
      .limit(100)
      .populate("user");

    return res.json(answers);
  } catch ({ message, ...error }) {
    return res.status(400).json({
      success: false,
      message,
    });
  }
};

exports.deleteAnswer = async function (req, res) {
  try {
    let answerDb = await Answers.findById(req.params.answerId);

    if (!answerDb) {
      res.status(404).json({
        status: false,
        message: `Answer '${req.params.answerId} is not found`,
      });
    }

    await Answers.findByIdAndDelete(req.params.answerId);

    await Questions.findByIdAndUpdate(req.params.questionId, {
      $pull: { answers: req.params.answerId },
    });

    res.json({
      success: true,
    });
  } catch ({ message, ...error }) {
    return res.status(400).json({
      success: false,
      message: `Cannot delete question: ${message}`,
    });
  }
};
