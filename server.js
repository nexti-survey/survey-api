/* eslint-disable no-unused-vars */
const dotenv = require("dotenv"),
  express = require("express"),
  cors = require("cors"),
  mongoose = require("mongoose");

dotenv.config();

const app = express();
const port = process.env.PORT || 3100;

// Add models dependencies to mongoose recognize it as model
const Users = require("./models/usersModel");
const Questions = require("./models/questionsModel");
const Answers = require("./models/answersModel");

mongoose.Promise = global.Promise;
mongoose.connect(
  `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PWD}@${process.env.MONGO_HOST}/survey?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.use(express.json());

app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.set("json spaces", 2);

app.use(function (err, req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept,X-Auth-Token"
  );
  res.header(
    "Access-Control-Allow-Methods",
    "PUT, PATCH, POST, GET, DELETE, OPTIONS"
  );

  if (err instanceof SyntaxError && err.status === 400 && "body" in err) {
    console.error(err);
    return res
      .status(400)
      .send({ success: false, error: `${err.message}. Content: ${err.body}` }); // Bad request
  }

  next();
});

require("./routes")(app);

app.route("/").get(function (req, res) {
  res.json({ message: "Survey API" });
});

app.use(function (req, res) {
  res.status(404);

  res.status(400).json({ error: `${req.method} ${req.path} is not found` });
  return;
});

app.listen(port);
console.log("Survey API server started on: " + port);
