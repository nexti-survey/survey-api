"use strict";
module.exports = function (app) {
  const user = require("../controllers/usersController");
  const question = require("../controllers/questionsController");
  const answer = require("../controllers/answersController");

  app
    .route("/questions")
    .get([user.checkToken, question.getQuestions])
    .post([user.checkToken, question.createQuestion]);

  app
    .route("/questions/:questionId([0-9a-fA-F]{24})")
    .get([user.checkToken, question.questionDetails])
    .put([user.checkToken, question.updateQuestion]);

  app
    .route("/questions/:questionId([0-9a-fA-F]{24})/answers")
    .get([user.checkToken, answer.getAnswers])
    .post([user.checkToken, answer.reply]);

  app
    .route("/questions/:questionId([0-9a-fA-F]{24})/answers/:answerId")
    .delete([user.checkToken, question.updateQuestion]);
};
