"use strict";
module.exports = function (app) {
  const user = require("../controllers/usersController");

  app.route("/users/login").post(user.login);

  app
    .route("/users")
    .post(user.createUser)
    .get([user.checkToken, user.userDetails]);

  app
    .route("/users/:userId([0-9a-fA-F]{24})")
    .put([user.checkToken, user.updateUser]);
};
